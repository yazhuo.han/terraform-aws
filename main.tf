provider "aws" { # provider exposes the entire API
    region = "us-east-2"

}

/**variable "subnet_cidr_block" {
    description = "subnet cidr block"
    default = "10.0.10.0/24"
    type = string
} **/

/**variable "vpc_cidr_block" {
    description = "vpc cidr block"
    # type = list(string)
} **/

# 3 ways to pass value to a variable
# 1. leave blank in file, and use cli
# terraform apply--> prompt for the input (for testing it's okay, but not very efficient)

# 2. cli
# terraform apply -var "<name-of-variable>=<value-of-variable>"

# 3. variables file
# terraform.tfvars


variable avail_zone{}

variable "cidr_blocks" {
    description = "cidr blocks for vpc and subnet"
    type = list(object({
        cidr_block = string
        name = string
    }))
}

resource "aws_vpc" "demoapp-vpc" {
    # cidr_block = "10.0.0.0/16"
    # cidr_block = var.vpc_cidr_block
    # cidr_block = var.vpc_cidr_block[0]
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        # Name: "development",
        Name: var.cidr_blocks[0].name, 
        vpc_env: "dev"
    }
}

resource "aws_subnet" "demoapp-subnet-1" {
    vpc_id = aws_vpc.demoapp-vpc.id # subnet resource is completely created based on vpc
    # cidr_block = "10.0.10.0/24"
    # cidr_block = var.subnet_cidr_block
    cidr_block = var.cidr_blocks[1].cidr_block
    # availability_zone = "us-east-2a"
    availability_zone = var.avail_zone
    tags = {
        # Name: "subnet-1-demoapp"
        Name: var.cidr_blocks[1].name
    }
}

data "aws_vpc" "existing_vpc" {
    # arguments = filter for query
    default = true
}


output "dev_vpc_id" {
    value = aws_vpc.demoapp-vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.demoapp-subnet-1.id
}